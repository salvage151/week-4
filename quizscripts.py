#Liam "McProbee" Wilson
#9/22/15
#Quiz 2

#Function 1
#___________________________________
'''
x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picCH)
def whiteBlue():
  for pixel in getPixels(picture):
    blueIs = getBlue(pixel)
    if(blueIs < 150):
      setColor(pixel, white)
    
whiteBlue()
show(picture) 
'''
#Function 2
#___________________________________
'''
x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picChore)
def changeValues():
  for pixel in getPixels(picture):
    blueIs = getBlue(pixel) * 2
    setBlue(pixel, blueIs)
    redIs = getRed(pixel) / 2
    setRed(pixel, redIs)
    greenIs = getGreen(pixel) / 2
    setGreen(pixel, greenIs)
   
    
changeValues()
show(picture)
'''
#Function 3
#___________________________________
'''
x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picChore)
def changeValues():
  for pixel in getPixels(picture):
    blueIs = getBlue(pixel) / 2
    setBlue(pixel, blueIs)
    redIs = getRed(pixel) * 2
    setRed(pixel, redIs)
    greenIs = getGreen(pixel) / 2
    setGreen(pixel, greenIs)
   
    
changeValues()
show(picture)
'''
#Function 4
#___________________________________
'''
x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picChore)
def greyDownScale(picture):
  for p in getPixels(picture):
    intensity = (getRed(p)+getGreen(p)+getBlue(p)) / 2
    setColor(p,makeColor(intensity,intensity,intensity))
def greyUpScale(picture):
  for px in getPixels(picture):
    red=getRed(px)
    green=getGreen(px)
    blue=getBlue(px)
    negColor=makeColor(255-red, 255-green, 255-blue)
    setColor(px, negColor)
    
greyDownScale(picture)
greyUpScale(picture)
show(picture)
'''
#Function 5
#___________________________________
'''

x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picChore)
def changeValues():
  for pixel in getPixels(picture):
    blueIs = getBlue(pixel) + 75
    setBlue(pixel, blueIs)
    redIs = getRed(pixel) + 75
    setRed(pixel, redIs)
    greenIs = getGreen(pixel) + 75
    setGreen(pixel, greenIs)
   
    
changeValues()
show(picture)
'''
#Function 6
#___________________________________
'''
x = 0
y = 0
picChore = pickAFile()
picture = makePicture(picChore)
xMax = getWidth(picture)
yMax = getHeight(picture) /2
def topHalfBlack():
  for y in range(0,yMax):
    y += 1
    x = 0
    for x in range(0,xMax):
      pixel = getPixelAt(picture, x, y)
      setColor(pixel, black)

topHalfBlack()
show(picture)
'''
#Function 7
#___________________________________

'''
picChore = pickAFile()
picture = makePicture(picChore)

def topHalfMirror(picture):
  height = getHeight(picture)
  halfHeight = height /2
  width = getWidth(picture)
 
 
  for y in range(halfHeight, height):
    for x in range(0, width):
      newY = height - 1 - y
      pixel1 = getPixelAt(picture, x , y)
      color1 = getColor(pixel1)
      pixel2 = getPixelAt(picture, x, newY)
      setColor(pixel2, color1)
   
 
    
topHalfMirror(picture)
show(picture)
'''